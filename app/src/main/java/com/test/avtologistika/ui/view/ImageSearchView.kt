package com.test.avtologistika.ui.view

interface ImageSearchView : AbstractView {
    fun setupUI()
    fun setupRecyclerView()
}