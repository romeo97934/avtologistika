package com.test.avtologistika.ui.presenter

import com.test.avtologistika.ui.view.AbstractView

abstract class AbstractPresenter<V : AbstractView> {
    lateinit var view: V

    open fun bind(view: V) {
        this.view = view
        this.view.onPresenterPostBind()
    }
}