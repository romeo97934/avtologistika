package com.test.avtologistika.ui.presenter

import android.annotation.SuppressLint
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.test.avtologistika.R
import com.test.avtologistika.data.model.LocalStoredImage
import com.test.avtologistika.data.model.SearchItem
import com.test.avtologistika.data.repository.ImageSearchRepository
import java.util.*

class ImageSearchPresenterImpl(private val repository: ImageSearchRepository) : ImageSearchPresenter() {
    override val locationRequest = LocationRequest().apply {
        interval = 4000
        fastestInterval = 2000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    override val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return
            lastLocation = locationResult.locations[0]
        }
    }

    @SuppressLint("CheckResult")
    override fun search(query: String) {
        repository.search(query)
                .subscribe({ items ->
                    updateAdapterItems(items)
                    repository.save(prepareLocalStoredModels(items)).subscribe {}
                }, {
                    view.snackBar(R.string.error)
                })
    }

    override fun prepareLocalStoredModels(items: List<SearchItem>): List<LocalStoredImage> =
            items.map {
                LocalStoredImage(it.title,
                        it.link,
                        lastLocation?.latitude ?: 0.0,
                        lastLocation?.longitude ?: 0.0,
                        Calendar.getInstance().timeInMillis)
            }

    override fun updateAdapterItems(items: List<SearchItem>) {
        adapter.setItems(items)
    }
}