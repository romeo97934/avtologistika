package com.test.avtologistika.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.avtologistika.R
import com.test.avtologistika.data.model.SearchItem
import com.test.avtologistika.ui.holder.ImageSearchViewHolder
import com.test.avtologistika.util.debug
import com.test.avtologistika.util.inflate
import com.test.avtologistika.util.load

class ImageSearchAdapter(private val items: ArrayList<SearchItem>) : RecyclerView.Adapter<ImageSearchViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageSearchViewHolder =
            ImageSearchViewHolder(parent.inflate(R.layout.item_image))

    override fun getItemCount() = items.count()

    override fun onBindViewHolder(holder: ImageSearchViewHolder, position: Int) {
        val item = items[position]

        holder.ivCover.load(item.link)
    }

    fun setItems(items: List<SearchItem>) = this.items.apply {
        clear()
        addAll(items)
        notifyDataSetChanged()
    }
}