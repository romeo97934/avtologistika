package com.test.avtologistika.ui.component

import com.test.avtologistika.ui.activity.ImageSearchActivity
import com.test.avtologistika.ui.module.ActivityModule
import com.test.avtologistika.ui.module.ImageSearchModule
import dagger.Component
import dagger.Module

@Component(modules = [ActivityModule::class, ImageSearchModule::class])
interface ImageSearchComponent {
    fun inject(activity: ImageSearchActivity)
}