package com.test.avtologistika.ui.view

import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.test.avtologistika.ui.activity.AbstractActivity

interface AbstractView {
    /**
     * Activity instance
     */
    val activity: AbstractActivity

    /**
     * Id of root view in activity layout
     */
    val rootView: Int

    /**
     * Call after [presenter] was successfully bind
     */
    fun onPresenterPostBind()

    /**
     * Show snackbar. It will be attached to [rootView]
     */
    fun snackBar(message: Int) {
        Snackbar.make(activity.findViewById<View>(rootView), message, Snackbar.LENGTH_SHORT)
    }
}