package com.test.avtologistika.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.test.avtologistika.R
import com.test.avtologistika.ui.component.DaggerImageSearchComponent
import com.test.avtologistika.ui.module.ActivityModule
import com.test.avtologistika.ui.module.ImageSearchModule
import com.test.avtologistika.ui.presenter.ImageSearchPresenter
import com.test.avtologistika.ui.view.ImageSearchView
import com.test.avtologistika.util.AppPermissionUtil
import com.test.avtologistika.util.debug
import com.test.avtologistika.util.hideKeyboard
import kotlinx.android.synthetic.main.activity_image_search.*
import javax.inject.Inject

class ImageSearchActivity : AbstractActivity(), ImageSearchView {
    @Inject
    override lateinit var appPermissionUtil: AppPermissionUtil

    @Inject
    lateinit var presenter: ImageSearchPresenter

    override val activity: AbstractActivity = this
    override val rootView: Int = R.id.root
    override val layoutId: Int = R.layout.activity_image_search
    override val neededPermissions: Array<String> = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.image_search_menu, menu)

        (menu.findItem(R.id.search_bar).actionView as SearchView).apply {
            queryHint = getString(R.string.hint_search)
            isIconified = false
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    presenter.search(query ?: "")
                    this@ImageSearchActivity.hideKeyboard()
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }
            })
        }

        return true
    }

    override fun onPermissionGranted() = presenter.bind(this)

    override fun injectDependencies() {
        DaggerImageSearchComponent.builder()
                .activityModule(ActivityModule(this))
                .imageSearchModule(ImageSearchModule(this))
                .build()
                .inject(this)
    }

    override fun onPresenterPostBind() {
        setupUI()
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        LocationServices.getFusedLocationProviderClient(this)
                .requestLocationUpdates(presenter.locationRequest, presenter.locationCallback, null)
    }

    override fun onStop() {
        super.onStop()
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(presenter.locationCallback)
    }

    override fun setupUI() {
        setupRecyclerView()
    }

    override fun setupRecyclerView() {
        recycler_view.apply {
            layoutManager = LinearLayoutManager(this@ImageSearchActivity)
            adapter = presenter.adapter
        }
    }
}