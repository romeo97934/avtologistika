package com.test.avtologistika.ui.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_image.view.*

class ImageSearchViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val ivCover = view.iv_cover
}