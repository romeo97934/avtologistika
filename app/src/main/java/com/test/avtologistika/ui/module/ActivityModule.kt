package com.test.avtologistika.ui.module

import com.test.avtologistika.ui.activity.AbstractActivity
import com.test.avtologistika.util.AppPermissionUtil
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private var activity: AbstractActivity) {
    @Provides
    fun provideAppPermissionUtil() = AppPermissionUtil(activity)
}