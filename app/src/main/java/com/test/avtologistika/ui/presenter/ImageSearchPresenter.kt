package com.test.avtologistika.ui.presenter

import android.location.Location
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.test.avtologistika.data.model.LocalStoredImage
import com.test.avtologistika.data.model.SearchItem
import com.test.avtologistika.ui.adapter.ImageSearchAdapter
import com.test.avtologistika.ui.view.ImageSearchView

abstract class ImageSearchPresenter : AbstractPresenter<ImageSearchView>() {
    val adapter = ImageSearchAdapter(ArrayList())
    var lastLocation: Location? = null
    abstract val locationRequest: LocationRequest
    abstract val locationCallback: LocationCallback

    abstract fun search(query: String)
    abstract fun updateAdapterItems(items: List<SearchItem>)
    abstract fun prepareLocalStoredModels(items: List<SearchItem>): List<LocalStoredImage>
}