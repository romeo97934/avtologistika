package com.test.avtologistika.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.avtologistika.util.AppPermissionUtil

/**
 * Base class, which represents permission checking logic
 */
abstract class AbstractActivity : AppCompatActivity() {
    /**
     * Do dependency injection
     */
    abstract fun injectDependencies()

    /**
     * Call after permissions were successfully granted
     */
    abstract fun onPermissionGranted()

    /**
     * Id of [AppCompatActivity] layout
     */
    abstract val layoutId: Int
    abstract var appPermissionUtil: AppPermissionUtil

    /**
     * [Array] of permissions, which current [AppCompatActivity] is requires
     */
    abstract val neededPermissions: Array<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        injectDependencies()
        checkPermission()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // Delegate result to support class
        appPermissionUtil.onRequestPermissionsResult(requestCode) { onPermissionGranted() }
    }

    // PRIVATE METHOD SECTION

    /**
     * Check permissions. If permissions was granted - [onPermissionGranted] will be executed, prompt dialog will be shown otherwise
     */
    private fun checkPermission() = with(appPermissionUtil) {
        load(*neededPermissions)
        check { onPermissionGranted() }
    }
}