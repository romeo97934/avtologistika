package com.test.avtologistika.ui.module

import com.test.avtologistika.data.repository.ImageSearchRepository
import com.test.avtologistika.ui.activity.AbstractActivity
import com.test.avtologistika.ui.presenter.ImageSearchPresenter
import com.test.avtologistika.ui.presenter.ImageSearchPresenterImpl
import dagger.Module
import dagger.Provides

@Module
class ImageSearchModule(private var activity: AbstractActivity) {
    @Provides
    fun provideRepository() = ImageSearchRepository(activity)

    @Provides
    fun providePresenter(): ImageSearchPresenter = ImageSearchPresenterImpl(provideRepository())
}