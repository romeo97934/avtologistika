package com.test.avtologistika.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.test.avtologistika.BuildConfig
import java.io.File

fun AppCompatActivity.replaceFragment(@IdRes container: Int, fragment: Fragment) {
    supportFragmentManager
            .beginTransaction()
            .replace(container, fragment)
            .commit()
}

fun AppCompatActivity.replaceFragment(@IdRes container: Int, fragment: Fragment, onComplete: () -> Unit) {
    supportFragmentManager
            .beginTransaction()
            .replace(container, fragment)
            .runOnCommit(onComplete)
            .commit()
}

fun AppCompatActivity.removeFragment(fragment: Fragment) {
    supportFragmentManager
            .beginTransaction()
            .remove(fragment)
            .commit()
}

fun <T : View> AppCompatActivity.inflate(id: Int, root: ViewGroup? = null): T {
    return LayoutInflater.from(this).inflate(id, root) as T
}

fun AppCompatActivity.hideKeyboard() {
    currentFocus?.apply {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(this.windowToken, 0)
    }
}

fun Any.debug(message: String) {
    if (BuildConfig.DEBUG)
        Log.d(this::class.java.name, message)
}

fun Any.debug(clazz: Any?) {
    if (!BuildConfig.DEBUG)
        return

    if (clazz == null)
        Log.d(this::class.java.name, "NULL")
    else
        Log.d(this::class.java.name, clazz.toString())
}

fun Any.debug(files: Array<File>?) {
    if (!BuildConfig.DEBUG)
        return

    if (files == null)
        Log.d(this::class.java.name, "NULL")
    else
        files.forEach {
            Log.d(this::class.java.name, it.name)
        }
}


fun ImageView.load(url: String) {
    Picasso.with(this.context).load(url).into(this)
}

fun ImageView.load(uri: Uri) {
    Picasso.with(this.context).load(uri).into(this)
}

fun ImageView.load(file: File) {
    Picasso.with(this.context).load(file).into(this)
}

fun ImageView.load(@DrawableRes drawable: Int) {
    Picasso.with(this.context).load(drawable).into(this)
}

inline fun ImageView.load(@DrawableRes drawable: Int, crossinline success: () -> Unit) {
    Picasso.with(this.context)
            .load(drawable)
            .into(this, object : Callback {
                override fun onSuccess() {
                    success()
                }

                override fun onError() {

                }
            })
}

inline fun ImageView.load(@DrawableRes drawable: Int, crossinline success: () -> Unit, crossinline error: () -> Unit) {
    Picasso.with(this.context)
            .load(drawable)
            .into(this, object : Callback {
                override fun onSuccess() {
                    success()
                }

                override fun onError() {
                    error()
                }
            })
}

inline fun ImageView.load(file: File, crossinline success: () -> Unit, crossinline error: () -> Unit) {
    Picasso.with(this.context)
            .load(file)
            .into(this, object : Callback {
                override fun onSuccess() {
                    success()
                }

                override fun onError() {
                    error()

                }
            })
}

inline fun Context.loadIntoTarget(link: String, crossinline error: () -> Unit, crossinline success: (bitmap: Bitmap?) -> Unit) {
    val target = object : Target {
        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
            debug("onPrepareLoad111")
        }

        override fun onBitmapFailed(errorDrawable: Drawable?) {
            debug("onBitmapFailed111")
            error()
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            debug("onBitmapLoaded111")
            success(bitmap)
        }
    }

    Picasso.with(this)
            .load(link)
            .into(target)
}

fun ViewGroup.inflate(@LayoutRes layout: Int) = LayoutInflater.from(context).inflate(layout, this, false)

fun Context.externalImageStorage() = getExternalFilesDir(Environment.DIRECTORY_DCIM)