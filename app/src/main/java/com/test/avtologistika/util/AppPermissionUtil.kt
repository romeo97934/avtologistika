package com.test.avtologistika.util

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class AppPermissionUtil(private val activity: AppCompatActivity) {
    companion object {
        const val PERMISSION_REQUEST_CODE = 91
    }

    private val permissions = ArrayList<String>()

    /**
     * Load permission list for future check. Old permissions will be cleared.
     * Should call this before {@link AppPermissionUtil#check}
     *
     * @param permissions - permissions for loading
     */
    fun load(vararg permissions: String) {
        this.permissions.clear()
        this.permissions.addAll(permissions.toList())
    }

    /**
     * Start permissions check process. If permissions is already granted, {@link onGranted} will be executed,
     * otherwise permission request dialog will be shown
     * @param onGranted
     */
    fun check(onGranted: () -> Unit) {
        if (checkIfAlreadyGranted(permissions.toList())) {
            onGranted()
            return
        }

        ActivityCompat.requestPermissions(activity, permissions.toTypedArray(), PERMISSION_REQUEST_CODE)
    }

    /**
     * Interceptor for {@link AppCompatActivity#onRequestPermissionsResult}.
     * Execute permissions checking after user interaction with prompt dialog.
     *
     * @param onGranted - will be executed if permissions was granted
     * @param requestCode - Activity request code
     */
    fun onRequestPermissionsResult(requestCode: Int, onGranted: () -> Unit) {
        if (requestCode == PERMISSION_REQUEST_CODE)
            check(onGranted)
    }

    /**
     * Check if permissions was already granted
     */
    private fun checkIfAlreadyGranted(permissions: List<String>): Boolean {
        for (perm in permissions)
            if (ContextCompat.checkSelfPermission(activity, perm) != PackageManager.PERMISSION_GRANTED)
                return false

        return true
    }
}