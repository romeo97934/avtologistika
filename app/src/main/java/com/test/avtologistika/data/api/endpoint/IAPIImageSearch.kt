package com.test.avtologistika.data.api.endpoint

import com.test.avtologistika.data.model.SearchResult
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface IAPIImageSearch {
    @GET("customsearch/v1")
    fun search(@Query("key") key: String,
               @Query("cx") cx: String,
               @Query("q") query: String,
               @Query("searchType") searchType: String = "image",
               @Query("num") num: Int = 10): Observable<SearchResult>
}