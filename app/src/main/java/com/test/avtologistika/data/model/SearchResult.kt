package com.test.avtologistika.data.model

data class SearchResult(val items: List<SearchItem>)