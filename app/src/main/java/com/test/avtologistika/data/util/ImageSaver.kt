package com.test.avtologistika.data.util

import android.content.Context
import android.graphics.Bitmap
import android.media.ExifInterface
import com.test.avtologistika.util.debug
import com.test.avtologistika.util.externalImageStorage
import java.io.File
import java.io.FileOutputStream
import java.util.*

object ImageSaver {
    fun save(context: Context, title: String, bitmap: Bitmap, lat: Double, lng: Double) {
        val file = File(context.externalImageStorage(), "$title-${Calendar.getInstance().timeInMillis}.png")
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, FileOutputStream(file))

        debug(file.absolutePath)

        ExifInterface(file.absolutePath).apply {
            setAttribute(ExifInterface.TAG_GPS_LATITUDE, "$lat")
            setAttribute(ExifInterface.TAG_GPS_LONGITUDE, "$lng")
        }
    }
}