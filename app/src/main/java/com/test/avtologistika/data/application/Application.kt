package com.test.avtologistika.data.application

import androidx.multidex.MultiDexApplication
import io.realm.Realm

class Application : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
}