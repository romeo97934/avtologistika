package com.test.avtologistika.data.model

data class SearchItem(val title: String,
                      val link: String)