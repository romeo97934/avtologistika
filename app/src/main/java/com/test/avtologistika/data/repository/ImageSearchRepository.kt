package com.test.avtologistika.data.repository

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.test.avtologistika.R
import com.test.avtologistika.data.api.API
import com.test.avtologistika.data.api.endpoint.IAPIImageSearch
import com.test.avtologistika.data.model.LocalStoredImage
import com.test.avtologistika.data.model.SearchItem
import com.test.avtologistika.data.util.ImageSaver
import com.test.avtologistika.util.debug
import com.test.avtologistika.util.loadIntoTarget
import com.vicpin.krealmextensions.saveAll
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.target.SimpleTarget

class ImageSearchRepository(val context: Context) {
    fun search(query: String): Observable<List<SearchItem>> =
            API.use(IAPIImageSearch::class.java)
                    .search(context.getString(R.string.google_api_key),
                            context.getString(R.string.google_api_cx),
                            query)
                    .map { it.items }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    @SuppressLint("CheckResult")
    fun save(items: List<LocalStoredImage>): Observable<Unit> {
        for (image in items) {
            // Hold strong references to variables
            val lat = image.lat
            val lng = image.lng

            Observable.fromCallable {
                debug(image.link)

                (Glide.with(context)
                        .load(image.link)
                        .submit()
                        .get() as BitmapDrawable).bitmap
            }
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe({
                        debug("dsfdgbfhg")
                        ImageSaver.save(context, image.name, it, lat, lng)
                    }, {})
        }

        return Observable.fromCallable { items.saveAll() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}