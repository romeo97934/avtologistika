package com.test.avtologistika.data.model

import io.realm.RealmObject

open class LocalStoredImage(var name: String = "",
                            var link: String = "",
                            var lat: Double = 0.0,
                            var lng: Double = 0.0,
                            var date: Long = 0L) : RealmObject(){
    override fun toString(): String {
        return "LocalStoredImage(name='$name', link='$link', lat=$lat, lng=$lng, date=$date)"
    }
}